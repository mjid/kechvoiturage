<?php

namespace App\Controller;

use App\Entity\Trip;
use App\Entity\TripPassager;
use App\Form\TripType;
use App\Repository\TripRepository;
use function PHPSTORM_META\elementType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/app/trip")
 */
class TripController extends Controller
{
    /**
     * @Route("/", name="trip_index", methods="GET")
     */
    public function index(TripRepository $tripRepository): Response
    {
        return $this->render('trip/index.html.twig', ['trips' => $tripRepository->findAll()]);
    }

    /**
     * @Route("/new", name="trip_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $trip = new Trip();
        $trip->setOwner($this->getUser());
        $form = $this->createForm(TripType::class, $trip);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if(!$form->getData()->getVehicule()) {
                $this->addFlash('danger', 'You need to first add a Vehicule!');
                return $this->redirectToRoute('vehicule_new');
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->flush();

            return $this->redirectToRoute('my_trips');
        }

        return $this->render('trip/new.html.twig', [
            'trip' => $trip,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="trip_show", methods="GET")
     */
    public function show(Trip $trip): Response
    {
        return $this->render('trip/show.html.twig', ['trip' => $trip]);
    }

    /**
     * @Route("/{id}/edit", name="trip_edit", methods="GET|POST")
     */
    public function edit(Request $request, Trip $trip): Response
    {
        if ($this->getUser() == $trip->getOwner()) {
            $form = $this->createForm(TripType::class, $trip);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute('trip_show', ['id' => $trip->getId()]);
            }
        }
        else {
            $this->addFlash('danger', 'ACCESS DENIED!!');

            return $this->redirectToRoute('index');
        }

        return $this->render('trip/edit.html.twig', [
            'trip' => $trip,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="trip_delete", methods="DELETE")
     */
    public function delete(Request $request, Trip $trip): Response
    {
        if (
            $this->isCsrfTokenValid('delete'.$trip->getId(), $request->request->get('_token'))
        && $this->getUser() == $trip->getOwner()
        ) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trip);
            $em->flush();
        } else {
            $this->addFlash('error', 'ACCESS DENIED');
        }

        return $this->redirectToRoute('my_trips');
    }

    /**
     * @Route("/trip/{id}/reserve", name="trip_reserve")
     */
    public function reserver(Trip $trip, Request $request)
    {
        $form = $this->createFormBuilder()->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tripPassager = new TripPassager();
            $tripPassager->setTrip($trip);
            $tripPassager->setPassager($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($tripPassager);
            $em->flush();
            return $this->redirectToRoute('my_trips');
        }

        return $this->render('trip/reserve.html.twig', [
            'form' => $form->createView(),
            'trip' => $trip
        ]);
    }

    /**
     * @Route("/reservation/{id}", name="reservation_accept")
     */
    public function acceptReservation(TripPassager $tripPassager)
    {
        $trip = $tripPassager->getTrip();
        if($trip->getNbrPlace() <= 0) {
            $this->addFlash(
                'danger',
                "Il n'ya plus de place"
            );
        }
        else {
            $tripPassager->setIsAccepted(true);
            $trip->setNbrPlace($trip->getNbrPlace() - 1);

            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);

            $em->flush();
        }


        return $this->redirectToRoute('trip_show', ['id' => $tripPassager->getTrip()->getId()]);
    }

    /**
     * @Route("/reservation/{id}/deny", name="reservation_deny")
     */
    public function denyReservation(TripPassager $tripPassager, Request $request)
    {
        $trip = $tripPassager->getTrip();
        if ($this->isCsrfTokenValid('delete'.$tripPassager->getId(), $request->request->get('_token'))) {
            if ($tripPassager->getIsAccepted())
            {
                $trip->setNbrPlace($trip->getNbrPlace()+1);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($trip);
            $em->remove($tripPassager);
            $em->flush();
        }

        return $this->redirectToRoute('trip_show', ['id' => $trip->getId()]);
    }
}
