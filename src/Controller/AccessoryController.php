<?php

namespace App\Controller;

use App\Entity\Accessory;
use App\Form\AccessoryType;
use App\Repository\AccessoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/app/accessory")
 */
class AccessoryController extends Controller
{
    /**
     * @Route("/", name="accessory_index", methods="GET")
     */
    public function index(AccessoryRepository $accessoryRepository): Response
    {
        return $this->render('accessory/index.html.twig', ['accessories' => $accessoryRepository->findAll()]);
    }

    /**
     * @Route("/new", name="accessory_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $accessory = new Accessory();
        $form = $this->createForm(AccessoryType::class, $accessory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($accessory);
            $em->flush();

            return $this->redirectToRoute('accessory_index');
        }

        return $this->render('accessory/new.html.twig', [
            'accessory' => $accessory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="accessory_show", methods="GET")
     */
    public function show(Accessory $accessory): Response
    {
        return $this->render('accessory/show.html.twig', ['accessory' => $accessory]);
    }

    /**
     * @Route("/{id}/edit", name="accessory_edit", methods="GET|POST")
     */
    public function edit(Request $request, Accessory $accessory): Response
    {
        $form = $this->createForm(AccessoryType::class, $accessory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('accessory_edit', ['id' => $accessory->getId()]);
        }

        return $this->render('accessory/edit.html.twig', [
            'accessory' => $accessory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="accessory_delete", methods="DELETE")
     */
    public function delete(Request $request, Accessory $accessory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$accessory->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($accessory);
            $em->flush();
        }

        return $this->redirectToRoute('accessory_index');
    }
}
