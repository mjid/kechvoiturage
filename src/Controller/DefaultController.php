<?php

namespace App\Controller;

use App\Entity\Addresse;
use App\Form\AddresseType;
use App\Repository\AddresseRepository;
use App\Repository\TripRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(AddresseRepository $addresseRepository)
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/search", name="search", methods={"POST"})
     */
    public function search(Request $request, TripRepository $tripRepository)
    {
        $form = $this->createFormBuilder()
            ->add('depart', EntityType::class, [
                'required' => true,
                'class' => Addresse::class,
                'choice_label' => 'name',
                'label' => "Départ"
            ])->add('destination', EntityType::class, [
                'required' => false,
                'class' => Addresse::class,
                'choice_label' => 'name',
                'label' => "Destination"
            ])->add('date', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date'
            ])->getForm();
        $form->handleRequest($request);

        if($request->isMethod("POST")) {
            $data = $form->getData();
            $depart = $data['depart'];
            $destionation = $data['destination'];
            $date = $data['date'];
            $trips = $tripRepository->search($depart, $destionation, $date);

            return $this->render('trip/index.html.twig', [
                'trips' => $trips
            ]);
        }

        return $this->render('default/_search-form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
