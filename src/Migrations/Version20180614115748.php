<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180614115748 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accessory (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE addresse (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rate (id INT AUTO_INCREMENT NOT NULL, rater_id INT DEFAULT NULL, rating_id INT DEFAULT NULL, rate INT NOT NULL, message LONGTEXT DEFAULT NULL, INDEX IDX_DFEC3F393FC1CD0A (rater_id), INDEX IDX_DFEC3F39A32EFC6 (rating_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip (id INT AUTO_INCREMENT NOT NULL, a_from_id INT DEFAULT NULL, a_to_id INT DEFAULT NULL, vehicule_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, aller DATETIME NOT NULL, retour DATETIME DEFAULT NULL, INDEX IDX_7656F53BC4B56656 (a_from_id), INDEX IDX_7656F53BBB629F75 (a_to_id), INDEX IDX_7656F53B4A4A3511 (vehicule_id), INDEX IDX_7656F53B7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trip_user (trip_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_A6AB4522A5BC2E0E (trip_id), INDEX IDX_A6AB4522A76ED395 (user_id), PRIMARY KEY(trip_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, tel VARCHAR(15) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE vehicule (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, marque VARCHAR(255) NOT NULL, modele VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, couleur VARCHAR(255) NOT NULL, p_immatriculation DATETIME DEFAULT NULL, seats INT NOT NULL, is_max2back_seats TINYINT(1) DEFAULT NULL, INDEX IDX_292FFF1D7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE rate ADD CONSTRAINT FK_DFEC3F393FC1CD0A FOREIGN KEY (rater_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rate ADD CONSTRAINT FK_DFEC3F39A32EFC6 FOREIGN KEY (rating_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BC4B56656 FOREIGN KEY (a_from_id) REFERENCES addresse (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BBB629F75 FOREIGN KEY (a_to_id) REFERENCES addresse (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B4A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip_user ADD CONSTRAINT FK_A6AB4522A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip_user ADD CONSTRAINT FK_A6AB4522A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vehicule ADD CONSTRAINT FK_292FFF1D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BC4B56656');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BBB629F75');
        $this->addSql('ALTER TABLE trip_user DROP FOREIGN KEY FK_A6AB4522A5BC2E0E');
        $this->addSql('ALTER TABLE rate DROP FOREIGN KEY FK_DFEC3F393FC1CD0A');
        $this->addSql('ALTER TABLE rate DROP FOREIGN KEY FK_DFEC3F39A32EFC6');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B7E3C61F9');
        $this->addSql('ALTER TABLE trip_user DROP FOREIGN KEY FK_A6AB4522A76ED395');
        $this->addSql('ALTER TABLE vehicule DROP FOREIGN KEY FK_292FFF1D7E3C61F9');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B4A4A3511');
        $this->addSql('DROP TABLE accessory');
        $this->addSql('DROP TABLE addresse');
        $this->addSql('DROP TABLE rate');
        $this->addSql('DROP TABLE trip');
        $this->addSql('DROP TABLE trip_user');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE vehicule');
    }
}
