<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180616173309 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B7E3C61F9');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BBB629F75');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BC4B56656');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BBB629F75 FOREIGN KEY (a_to_id) REFERENCES addresse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BC4B56656 FOREIGN KEY (a_from_id) REFERENCES addresse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip_passager DROP FOREIGN KEY FK_17A51DF971A51189');
        $this->addSql('ALTER TABLE trip_passager DROP FOREIGN KEY FK_17A51DF9A5BC2E0E');
        $this->addSql('ALTER TABLE trip_passager ADD CONSTRAINT FK_17A51DF971A51189 FOREIGN KEY (passager_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE trip_passager ADD CONSTRAINT FK_17A51DF9A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE vehicule DROP FOREIGN KEY FK_292FFF1D7E3C61F9');
        $this->addSql('ALTER TABLE vehicule ADD CONSTRAINT FK_292FFF1D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BC4B56656');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53BBB629F75');
        $this->addSql('ALTER TABLE trip DROP FOREIGN KEY FK_7656F53B7E3C61F9');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BC4B56656 FOREIGN KEY (a_from_id) REFERENCES addresse (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53BBB629F75 FOREIGN KEY (a_to_id) REFERENCES addresse (id)');
        $this->addSql('ALTER TABLE trip ADD CONSTRAINT FK_7656F53B7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE trip_passager DROP FOREIGN KEY FK_17A51DF9A5BC2E0E');
        $this->addSql('ALTER TABLE trip_passager DROP FOREIGN KEY FK_17A51DF971A51189');
        $this->addSql('ALTER TABLE trip_passager ADD CONSTRAINT FK_17A51DF9A5BC2E0E FOREIGN KEY (trip_id) REFERENCES trip (id)');
        $this->addSql('ALTER TABLE trip_passager ADD CONSTRAINT FK_17A51DF971A51189 FOREIGN KEY (passager_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE vehicule DROP FOREIGN KEY FK_292FFF1D7E3C61F9');
        $this->addSql('ALTER TABLE vehicule ADD CONSTRAINT FK_292FFF1D7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user (id)');
    }
}
