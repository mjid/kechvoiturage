<?php

namespace App\Form;

use App\Entity\Addresse;
use App\Entity\Trip;
use App\Entity\Vehicule;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TripType extends AbstractType
{
    protected $tokenStorage;
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('aller', DateTimeType::class, [
                'date_widget' => 'single_text',
                'required' => true
            ])
            ->add('aFrom', EntityType::class, [
                'class' => Addresse::class,
                'choice_label' => 'name',
                'label' => 'From',
                'required' => true
            ])
            ->add('aTo', EntityType::class, [
                'class' => Addresse::class,
                'choice_label' => 'name',
                'label' => 'To',
                'required' => true
            ])
            ->add('nbrPlace', IntegerType::class, [
                'label' => 'Nombre de place',
                'required' => true
            ])
            ->add('isMax2BackSeats', CheckboxType::class, [
                'label' => 'Set back seats to 2 maximum?',
                'required' => false
            ])
            ->add('price', IntegerType::class, [
                'label' => 'Price per place',
                'required' => true
            ])
            ->add('vehicule', EntityType::class, [
                'class' => Vehicule::class,
                'required' => true,
                'choice_label' => 'modele',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('v')
                        ->where('v.owner = ?1')
                        ->setParameter(1, $this->tokenStorage->getToken()->getUser());
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Trip::class,
            'error_bubbling' => true
        ]);
    }
}
