<?php

namespace App\DataFixtures;

use App\Entity\Accessory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AccessoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i < 4; $i++)
        {
            $accessory = new Accessory();
            $accessory->setName('Accessory ' . $i);
            $manager->persist($accessory);
        }

        $manager->flush();
    }
}
