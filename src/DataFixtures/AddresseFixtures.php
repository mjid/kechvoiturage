<?php

namespace App\DataFixtures;

use App\Entity\Addresse;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AddresseFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++)
        {
            $addresse = new Addresse();
            $addresse->setName('Addresse ' . $i);
            $manager->persist($addresse);
        }
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
