<?php

namespace App\Repository;

use App\Entity\Addresse;
use App\Entity\Trip;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Trip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trip[]    findAll()
 * @method Trip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Trip::class);
    }

    public function search(Addresse $depart, Addresse $destination = null, \DateTime $date)
    {
        $qb = $this->createQueryBuilder('t')
            ->where('t.aFrom = :val1')
            ->setParameter('val1', $depart);
        if($destination) {
            $qb->andWhere('t.aTo = :val2')
                ->setParameter('val2', $destination);
        }
        $qb->andWhere('t.aller >= :val3')
           ->setParameter('val3', $date);
        return
            $qb->getQuery()
                ->getResult()
            ;
            //
    }

//    /**
//     * @return Trip[] Returns an array of Trip objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Trip
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
