<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TripRepository")
 */
class Trip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $aller;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrPlace;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isMax2BackSeats;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Addresse")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $aFrom;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Addresse")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $aTo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicule")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $vehicule;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TripPassager", mappedBy="trip")
     */
    private $passagers;

    public function getId()
    {
        return $this->id;
    }

    public function getAFrom()
    {
        return $this->aFrom;
    }

    public function setAFrom($aFrom): self
    {
        $this->aFrom = $aFrom;

        return $this;
    }

    public function getAller(): ?\DateTimeInterface
    {
        return $this->aller;
    }

    public function setAller(\DateTimeInterface $aller): self
    {
        $this->aller = $aller;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getATo()
    {
        return $this->aTo;
    }

    /**
     * @param mixed $aTo
     */
    public function setATo($aTo): void
    {
        $this->aTo = $aTo;
    }

    /**
     * @return mixed
     */
    public function getVehicule()
    {
        return $this->vehicule;
    }

    /**
     * @param mixed $vehicule
     */
    public function setVehicule($vehicule): void
    {
        $this->vehicule = $vehicule;
    }

    /**
     * @return mixed
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    public function getPassagers()
    {
        return $this->passagers;
    }

    /**
     * @return mixed
     */
    public function getisMax2BackSeats()
    {
        return $this->isMax2BackSeats;
    }

    /**
     * @param mixed $isMax2BackSeats
     */
    public function setIsMax2BackSeats($isMax2BackSeats): void
    {
        $this->isMax2BackSeats = $isMax2BackSeats;
    }

    /**
     * @return mixed
     */
    public function getNbrPlace()
    {
        return $this->nbrPlace;
    }

    /**
     * @param mixed $nbrPlace
     */
    public function setNbrPlace($nbrPlace): void
    {
        $this->nbrPlace = $nbrPlace;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }
}
