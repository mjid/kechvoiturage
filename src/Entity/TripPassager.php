<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TripPassagerRepository")
 */
class TripPassager
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Trip", inversedBy="passagers")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $trip;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $passager;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAccepted;

    public function getId()
    {
        return $this->id;
    }

    public function getIsAccepted(): ?bool
    {
        return $this->isAccepted;
    }

    public function setIsAccepted(?bool $isAccepted): self
    {
        $this->isAccepted = $isAccepted;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTrip(): Trip
    {
        return $this->trip;
    }

    /**
     * @param mixed $trip
     */
    public function setTrip($trip): void
    {
        $this->trip = $trip;
    }

    /**
     * @return mixed
     */
    public function getPassager()
    {
        return $this->passager;
    }

    /**
     * @param mixed $passager
     */
    public function setPassager($passager): void
    {
        $this->passager = $passager;
    }
}
